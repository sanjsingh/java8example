package example.functional.dfault_static;

/*IT IS NOT A functional INTERFACE*/
public interface DefaultMethod {
	public String name();
	public String name(int id);

	default String getBossName() {

		return "Sanjay";
	}

	default String getBossName(int id) {

		return "Sanjay" + id;
	}

}
