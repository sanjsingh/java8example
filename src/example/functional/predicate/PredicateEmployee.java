package example.functional.predicate;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;


public class PredicateEmployee {

	/* All Employees who are male and age more than 21 */

	public static Predicate<Employee> getMaleEmployee() {
		return employee -> employee.getAge() > 21 && employee.getGender().equalsIgnoreCase("M");
	}

	/* All Employees who are female and age more than 18 */
	public static Predicate<Employee> getFemale() {
		return employee -> employee.getAge() > 18 && employee.getGender().equalsIgnoreCase("F");

	}
	/*All Employees whose age is more than a given age*/
	
	public static  Predicate<Employee>  getEmployeeIsMore(int age){
		return employee->employee.getAge()>age;
	}
	
	/*employees.stream().filter(predicate).collect(Collectors.toList());*/
	public static List<Employee> getAllList(List<Employee> employees,Predicate<Employee> predicate){
		
		return employees.stream().filter(predicate).collect(Collectors.toList());
		
	}

}
